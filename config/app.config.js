export default {
  mapboxToken: 'pk.eyJ1IjoiZGFubnlocm50IiwiYSI6ImNqNDRucXZrdTFhM3MycW14dTlobzZ3MWQifQ.8WqX54tvW8ChgiUiKfQEdg',
  mapStyles: {
    default: 'mapbox://styles/ryanhamley/ciuxgnhff00jo2irrvml8bfjk',
    satellite: 'mapbox://styles/mapbox/satellite-v9',
    streets: 'mapbox://styles/mapbox/streets-v9'
  }
}