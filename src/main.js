import Vue from 'vue';
// don't worry, we haven't created this yet!
// import AppComponent from './components/app-component/app-component';
import App from './pages/App.vue';
import Home from './pages/Home.vue';
import About from './pages/About.vue';
import Map from './pages/Map.vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import VueHead from 'vue-head'

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(VueHead)

// new Vue({
//   el: '#app',
//   components: {
//     'app-component': AppComponent
//   }
// });


// var router = new VueRouter;

// Set up routing and match routes to components

const routes = [
  	{ path: '/home', component: Home},
  	{ path: '/about', component: About},
  	{ path: '/map', component: Map},
  	{ path: '*', redirect: '/home' }
]


const router = new VueRouter({
  routes // short for `routes: routes`
})

const app = new Vue({
  router,
  render: h => h(App)
}).$mount('#app')


// Start the app on the #app div
// router.start(App, '#app');